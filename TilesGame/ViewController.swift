//
//  ViewController.swift
//  TilesGame
//
//  Created by Sierra 4 on 16/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    let picker = UIImagePickerController()
    @IBOutlet weak var imageViewOutlt: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         picker.delegate = self
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    @IBAction func SelectButtonAction(_ sender: Any) {
        
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
       
    }

    @IBAction func NextButtonAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Segue", sender: self)
    }
    
    
func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageViewOutlt.contentMode = .scaleAspectFit
        imageViewOutlt.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Segue"{
            let destina = segue.destination as! ImageController
           destina.SelectedImage = imageViewOutlt.image!        }
    }
    
    
    
}

