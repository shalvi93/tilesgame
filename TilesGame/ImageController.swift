//
//  ImageController.swift
//  cd 
//
//  Created by Sierra 4 on 16/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//
import UIKit
import CLTimer
class ImageController: UIViewController,cltimerDelegate{
    
    var SelectedImage = UIImage()
    var Counter = 0
    var Stimer = Timer()
    var oldFrame : CGRect = CGRect.zero
    
    @IBOutlet weak var timerOutlt: CLTimer!
    @IBOutlet var fullImageOutlt: [UIImageView]!
    @IBOutlet var RndomImgOutlt : [UIImageView]!
   
    var topLeft : UIImage!
    var topRight : UIImage!
    var topMiddle : UIImage!
    
    var bottomLeft : UIImage!
    var bottomRight : UIImage!
    var bottomcenter : UIImage!
    var verticallyMiddleHalf : UIImage!
    
    
   
    var middleleft : UIImage!
    var middlecenter : UIImage!
    var middleRight : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timerOutlt.startTimer(withSeconds: 20, format:.Minutes , mode: .Reverse)
        Stimer  = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction),
                                       userInfo: nil, repeats: true)
        
        let image : UIImage = SelectedImage
        
        let topHalf = image.topHalf
        let bottomHalf = image.bottomHalf
        let middleHalf = image.VerticallyMiddleHalf
        
     //   var arrayofimages = [topLeft,topMiddle,topRight,middleleft,middlecenter,middleRight, bottomLeft,bottomcenter ,bottomRight]
       
        
         topLeft = topHalf?.leftHalf
         topMiddle = topHalf?.horizontallyMiddleHalf
         topRight = topHalf?.rightHalf
        
         middleleft = middleHalf?.leftHalf
         middlecenter = middleHalf?.horizontallyMiddleHalf
         middleRight = middleHalf?.rightHalf
        
         bottomLeft = bottomHalf?.leftHalf
         bottomcenter = bottomHalf?.horizontallyMiddleHalf
         bottomRight = bottomHalf?.rightHalf
        
        for index in 0...8
        {
            RndomImgOutlt[index].layer.borderWidth = 2
            RndomImgOutlt[index].layer.borderColor = UIColor.gray.cgColor
        }

         RndomImgOutlt[0].image = bottomLeft
         RndomImgOutlt[2].image = bottomcenter
         RndomImgOutlt[3].image = bottomRight
         RndomImgOutlt[1].image = topLeft
         RndomImgOutlt[4].image = topMiddle
         RndomImgOutlt[5].image = topRight
         RndomImgOutlt[6].image = middleleft
         RndomImgOutlt[7].image = middlecenter
         RndomImgOutlt[8].image = middleRight
        
        var shufflingArr = [0 , 1 , 2 , 3, 4, 5 , 6, 7 , 8]
        shufflingArr.shuffle()
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[0].addGestureRecognizer(gestureRecognizer)
        let gestureRecognizer1 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[1].addGestureRecognizer(gestureRecognizer1)
        let gestureRecognizer2 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[2].addGestureRecognizer(gestureRecognizer2)
        let gestureRecognizer3 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[3].addGestureRecognizer(gestureRecognizer3)
        let gestureRecognizer4 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[4].addGestureRecognizer(gestureRecognizer4)
        let gestureRecognizer5 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[5].addGestureRecognizer(gestureRecognizer5)
        let gestureRecognizer6 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[6].addGestureRecognizer(gestureRecognizer6)
        let gestureRecognizer7 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[7].addGestureRecognizer(gestureRecognizer7)
        let gestureRecognizer8 = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.RndomImgOutlt[8].addGestureRecognizer(gestureRecognizer8)
        
        
        
        let panGesture10 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[0].addGestureRecognizer(panGesture10)
        let panGesture20 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[1].addGestureRecognizer(panGesture20)
        let panGesture30 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[2].addGestureRecognizer(panGesture30)
        let panGesture40 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[3].addGestureRecognizer(panGesture40)
        let panGesture50 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[4].addGestureRecognizer(panGesture50)
        let panGesture60 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[5].addGestureRecognizer(panGesture60)
        let panGesture70 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[6].addGestureRecognizer(panGesture70)
        let panGesture80 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[7].addGestureRecognizer(panGesture80)
        let panGesture90 = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.fullImageOutlt[8].addGestureRecognizer(panGesture90)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func handlePan(_ gestureRecognizer: UIPanGestureRecognizer)
    {
        let translation = gestureRecognizer.translation(in: self.view)
        let rec =  gestureRecognizer.view as! UIImageView
        self.view.bringSubview(toFront: rec)
        if (rec.image != nil)
        {
        rec.center = CGPoint(x:rec.center.x + translation.x,
                             y:rec.center.y + translation.y)
        switch gestureRecognizer.state
        {
        case .ended:
            var flag = false
            
            for i in 0...8{
                if (fullImageOutlt[i].frame.intersects(rec.frame)) {
                if (fullImageOutlt[i].image == nil)
                    {
                        fullImageOutlt[i].image = rec.image
                        rec.image = UIImage(named: "")
                        flag = true
                    }
                    else{
                        flag = false
                    }}}
            
            
            for i in 0...8{
                    if (RndomImgOutlt[i].frame.intersects(rec.frame)) {
                    if (RndomImgOutlt[i].image == nil)  {
                        RndomImgOutlt[i].image = rec.image
                  //      RndomImgOutlt.image = UIImage(named: "")
                        flag = true
                        }
                    else
                        {
                        flag = false
                    }}}
            if(!flag){
                rec.frame = oldFrame
            }
        case .began:
            oldFrame = rec.frame
        default:
            break  }
       gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
        }}

    
    
    func timerAction() {
        if(Counter <= 20)
        {
            Counter += 1
        }
        else{
            Stimer.invalidate()
            alertBox(message: " Click 'Reset' for another Game                 OR Exit")
        }}

    func alertBox(message : String)
    {
        let alert = UIAlertController(title: "Oops! Your Game is Over.❕", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
      }
    
    @IBAction func submitButton(_ sender: Any) {
        
        
        for i in 0...8
        {
            if (self.fullImageOutlt[i].image != nil)
            {
                if  ((fullImageOutlt[0].image == topLeft) &&
                    (fullImageOutlt[1].image == topMiddle) &&
                    (fullImageOutlt[2].image == topRight) &&
                   
                    (fullImageOutlt[3].image == middleleft) &&
                    (fullImageOutlt[4].image == middlecenter) &&
                    (fullImageOutlt[5].image == middleRight) &&
                   
                    (fullImageOutlt[6].image == bottomLeft) &&
                    (fullImageOutlt[7].image == bottomcenter) &&
                    (fullImageOutlt[8].image == bottomRight))
                {
                    alertBox(message: "Yippie!! You won 🤗")
                }
                else
                {
                    alertBox(message: "Better luck Next time 😞")
                }
                
            }
        }    }



    @IBAction func BackButtonActn(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
}

//----------------------------------------------------------------------------------------//
    extension UIImage {
    
    var topHalf: UIImage?
    {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: .zero,
                size: CGSize(width: size.width, height: size.height/3)))
            else{
            return nil
        }
        return UIImage(cgImage: image, scale: 1, orientation: imageOrientation)}
    
    
    var VerticallyMiddleHalf: UIImage?
    {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: CGPoint(x: 0,  y: CGFloat(Int(size.height)-Int(size.height/3)-Int(size.height/3))),
                size: CGSize(width: size.width, height: CGFloat(Int(size.height) - Int((size.height/3 )*2  )))))
            else{
            return nil
        }
        return UIImage(cgImage: image)}
    
   
    var bottomHalf: UIImage?
    {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: CGPoint(x: 0,  y: CGFloat(Int(size.height) - Int(size.height/3))),
                size: CGSize(width: size.width, height: CGFloat(Int(size.height) - Int((size.height/3)*2 )))))
            else{
            return nil
        }
        return UIImage(cgImage: image)}
    
    
    
    var leftHalf: UIImage?
    {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: .zero,
            size: CGSize(width: size.width/3, height: size.height)))
            else{
            return nil
        }
        return UIImage(cgImage: image)}
    
   
   
    var horizontallyMiddleHalf: UIImage?
    {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: CGPoint(x: CGFloat(Int(size.width)-Int((size.width/3))-Int((size.width/3))), y: 0),
            size: CGSize(width: CGFloat(Int(size.width)-Int(((size.width/3)*2))), height: size.height)))
            else{
            return nil
        }
        return UIImage(cgImage: image)}
    
   
    var rightHalf: UIImage?
    {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: CGPoint(x: CGFloat(Int(size.width)-Int((size.width/3))), y: 0),
            size: CGSize(width: CGFloat(Int(size.width)-Int(((size.width/3)*2))), height: size.height)))
            else{
            return nil
              }
        return UIImage(cgImage: image) }}


 extension Array
 {
    mutating func shuffle()
    {
        for _ in 0..<10
        {
            sort { (_,_) in arc4random() < arc4random() }
        }}}
